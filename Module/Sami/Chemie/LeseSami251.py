import serial
import time

ser = serial.Serial(
    port='COM6',
    baudrate=57600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=180
)

# print("com4 is " + str(ser.isOpen()))


ser.rts = 1

read_sc = ser.read_until(b'\r\r')

ser.rts = 0

ser.close()

read_qc = ":7000" + str(read_sc)[2:-1]  # :7000 necessary, so that 0A are on 11th and 12th position for QC_PH
read_sc = str(read_sc)[2:-1]   # plain string for SamiClient

#print(read_sc)
#print(read_qc)

#file = open("C:/kurth/Host/DATA/Sami/Sami251_QcPh.txt", 'a')
#file.write(read_qc + "\r")
#file.close()

file = open("C:/kurth/Host/DATA/Sami/Sami251_Client.txt", 'a')
file.write(read_sc + "\r")
file.close()

'''

file = open("C:/Users/FINO2/Documents/FINO2/SamiTest/double/Sami251_Client.txt", 'a')
file.write("Hallo 251" + "\r")
file.close()
'''