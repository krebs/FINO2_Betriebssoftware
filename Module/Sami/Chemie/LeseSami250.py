import serial
import time

ser = serial.Serial(
    port='COM1',
    baudrate=57600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=180
)

#print("com1 is " + str(ser.isOpen()))
ser.rts = 1

read = ser.read_until(b'\r\r')

ser.rts = 0

ser.close()

read_cl = ":7000" + str(read)[2:-1]  # :7000 necessary, so that 0A are on 11th and 12th position for QC_PH
read = str(read)[2:-1]  # plain string for SamiClient

# print(read)
# print(read_cl)

#file = open("C:/kurth/Host/DATA/Sami/Sami250_QcPh.txt", 'a')
#file.write(read_cl + "\r")
#file.close()

file = open("C:/kurth/Host/DATA/Sami/Sami250_Client.txt", 'a')
file.write(read + "\r")
file.close()
